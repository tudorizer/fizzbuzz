import sys

if len(sys.argv) >= 2:
    n = int(sys.argv[1])
else:
    n = raw_input("Enter n: ")
    n = int(n)

for i in range(1, n):
    result = ""
    result += "Fizz" * (1 - (i % 3))
    result += "Buzz" * (1 - (i % 5))
    result +=  str(i) * (1 - len(result))
    print result
    
                     